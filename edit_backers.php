<?php 



$backers_params = array();
$backers_params['limit'] = 50;
if(url_param('current_page')){
$backers_params['current_page'] = url_param('current_page');	
}
if(isset($params['keyword'])){
$backers_params['keyword'] =$params['keyword'];	
$backers_params['no_limit'] = true;

}
$backers = get_backers($backers_params);
$backers_params['page_count'] = true;
$pages_num = get_backers($backers_params); 

$paging_params = array();
$paging_params['num'] = $pages_num;

 
if(url_param('current_page')){
$paging_params['current_page'] = url_param('current_page');	
}

 
?>
<?php if($backers): ?>

<table width="100%" border="0" class="mw-ui-table" style="table-layout:fixed">
  <thead>
    <tr>
      <th>Backer Name</th>
      <th>Email</th>
      <th>Amount</th>
      <th>Visible</th>
      <th width="140px">&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($backers as $backer): ?>
    <?php //dd( $backer); ?>
    <tr id="baker-<?php print $backer['id']; ?>">
      <td><input type="text" class="mw-ui-field" name="backer_name" value="<?php print $backer['backer_name']; ?>" /></td>
      <td><input type="text" class="mw-ui-field" name="backer_email" value="<?php print $backer['backer_email']; ?>" /></td>
      <td><input type="text" class="mw-ui-field" name="backer_amount" value="<?php print $backer['backer_amount']; ?>" /></td>
      <td><select class="mw-ui-field mw-ui-field-medium" name="backer_visible">
          <option <?php if($backer['backer_visible'] == 1): ?> selected="selected" <?php endif; ?> value="1">Yes</option>
          <option value="0" <?php if($backer['backer_visible'] != 1): ?> selected="selected" <?php endif; ?>>No</option>
        </select></td>
      <td><input type="hidden" name="id" value="<?php print $backer['id']; ?>" />
        <button class="mw-ui-btn" onclick="edit_backer('#baker-<?php print $backer['id']; ?>')">Save</button>
        <a class="mw-ui-btn mw-ui-btn-icon" href="javascript:;" onclick="delete_backer('<?php print $backer['id']; ?>')"> <span class="mw-icon-bin"></span> </a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php if(!isset($params['keyword'])): ?>
<?php print paging($paging_params); ?>
<?php endif; ?>
<?php endif; ?>
