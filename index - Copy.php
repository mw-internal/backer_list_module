<div id="legend">
  <div class="mw-ui-row">
    <div class="mw-ui-col">
      <div class="mw-ui-col-container"> <span class="legendcircle amount-1"></span> <span>5$-30$</span> <span class="legendcircle amount-2"></span> <span>31$-50$</span> <span class="legendcircle amount-3"></span> <span>51$-100$</span> <span class="legendcircle amount-4"></span> <span>101$-500$</span> <span class="legendcircle amount-5"></span> <span>501$ ></span> </div>
    </div>
    <div class="mw-ui-col" style="width: 350px;">
      <div class="mw-ui-col-container"> <span id="join-title">Join this awesome people <a href="<?php print site_url() ?>">DONATE NOW</a></span> </div>
    </div>
  </div>
</div>
<?php $backers = get_backers("no_limit=true"); ?>
<?php if($backers): ?>
<div class="mw-ui-box mw-ui-box-content">
  <ul class="mw-bakers-list">
    <?php foreach($backers as $backer): ?>
    <?php if(trim($backer['backer_name'])): ?>
    <?php
        $cls = '1';

        $bv = intval($backer['backer_amount']);


        if($bv >= 5 and $bv <= 30){
          $cls = 1;
        }
        else if($bv >= 31 and $bv <= 50){
          $cls = 2;
        }
        else if($bv >= 51 and $bv <= 100){
           $cls = 3;
        }
        else if($bv >= 101 and $bv <= 500){
           $cls = 4;
        }
        else if($bv >= 501){
           $cls = 5;
        }

    ?>
    <li class="amount-<?php print $cls;  ?>"><?php print $backer['backer_name']; ?></li>
    <?php endif; ?>
    <?php endforeach; ?>
  </ul>
</div>
<?php else: ?>
<?php endif; ?>
