<style>

.mw-bakers-list span{

  -webkit-transition: all 0.2s;
  -moz-transition: all 0.2s;
  -ms-transition: all 0.2s;
  -o-transition: all 0.2s;
  transition: all 0.2s;
  border:2px solid transparent;
}
.nobkhidden{
	opacity: 1;
}
.bkhidden{
  opacity: 0;
/*
  -webkit-transform:scale(0);
  -moz-transform:scale(0);
  -ms-transform:scale(0);
  -o-transform:scale(0);
  transform:scale(0);
*/

  -webkit-transform:translateY(-30px);
  -moz-transform:translateY(-30px);
  -ms-transform:translateY(-30px);
  -o-transform:translateY(-30px);
  transform:translateY(-30px);
}

.mw-bakers-list span:hover{
  -webkit-transform:scale(1.4);
  -moz-transform:scale(1.4);
  -ms-transform:scale(1.4);
  -o-transform:scale(1.4);
  transform:scale(1.4);
  border:2px solid white;
}

</style>
<script>

bakers_current_page = 1;
bakers_keyword_old = '';
bakers_keyword = '';
bakers_reset = false;


showBackerSharer = function(id){
	if(id == false){
		var html = '';
	} else {
		var html = '<' +'div type="backers_list/share" id="backers-list-sharer" backer-id="'+id+'"'+'><'+'/div>';
	}
	$('#backer-share-modal').remove();
	//$('#backer-share-wrap').html(html);
	if(html != ''){
		
		
		
		mw.modal({
		content:   html,
		width:     500,
		height:    400,
	  
		template:  "basic", // "simple", "basic"  
		id:        'backer-share-modal'
	
		});
		mw.reload_module('#backers-list-sharer');
		 

		
		
	}
}



getBakers = function(){
   var url = mw.settings.api_url + 'get_backers_json';
   
   var last_page = 1;
   
   mw.one('getBakers', function(){
       $.post(url, {
            current_page:bakers_current_page,
            limit:200,
            keyword:bakers_keyword
       }, function(data){
          var html = '';
		  var backers_list  = data;
		  if(data.length == 1){
			  bakers_reset = true;
		  }
		  last_page = bakers_current_page;
		  
          $.each(data, function(){
			html += '<a href="#backer='+this.id+' ">';  
            html += '<span class="amount-'+this.level+' bkhidden">'+this.backer_name+'</span>';
			  if(data.length == 1){
			  html = '<span class="amount-'+this.level+' nobkhidden">'+this.backer_name+'</span>';
 
		  	}
			html += '</a>';
			
			
          });
		  
		
		  
		  
		  
          if(bakers_reset === false){
             $(".mw-bakers-list").append(html)
          }
          else{
             $(".mw-bakers-list").html(html);
             bakers_reset = false;
          }
          var time = 100;
          mw.$(".bkhidden").each(function(){
            time+=8;
            var el = $(this);
            (function(time,el){
                setTimeout(function(){
                    el.removeClass('bkhidden')
                }, time)
            })(time,el);
          });
       });
	   
	   
	   if(bakers_keyword.length == 0){
       setTimeout(function(){
         mw.one('getBakers' , 'ready');
		  
         var view = document.getElementById('bkview');
        if(mw.tools.inview(view)){
			  getBakers()
        }

       }, 777);
	   } else {
		 mw.one('getBakers' , 'ready');   
	   }
	   
	   
   });

}

$(document).ready(function(){
    getBakers();
    $("#searchbakers").bind('change keyup paste', function(){
       if(this.value == bakers_keyword_old){
         return false;
       }
       bakers_keyword = this.value;
       bakers_current_page = 1;
       bakers_reset = true;

       bakers_keyword_old = this.value;

       getBakers();


    });
    $(window).bind('load scroll resize', function(){
        var view = document.getElementById('bkview');
        if(mw.tools.inview(view)){
          mw.one('pageBakers', function(){
            bakers_current_page++;
            getBakers();
            setTimeout(function(){
                mw.one('pageBakers', 'ready');
            }, 777);
          });
        }
    });
});

</script>
<script>

$(document).ready(function(){
    mw.on.hashParam('backer', function(){
        if(this != false){
			showBackerSharer(this);
        } else {
			showBackerSharer(false);
		}
        
    });
     
});

</script>

<div id="backer-share-wrap"></div>

<div id="legend">
  <div class="mw-ui-row">
    <div class="mw-ui-col">
      <div class="mw-ui-col-container"> <span class="legendcircle amount-1"></span> <span>5$-30$</span> <span class="legendcircle amount-2"></span> <span>31$-50$</span> <span class="legendcircle amount-3"></span> <span>51$-100$</span> <span class="legendcircle amount-4"></span> <span>101$-500$</span> <span class="legendcircle amount-5"></span> <span>501$ ></span> </div>
    </div>
    <div class="mw-ui-col" style="width: 350px;">
      <div class="mw-ui-col-container"> <?php /*<span id="join-title">Join this awesome people <a href="<?php print site_url() ?>">DONATE NOW</a></span>*/ ?> <input type="text" class="mw-ui-field pull-right" id="searchbakers" placeholder="Search Backers" /></div>
    </div>
  </div>
</div>


<div class="mw-ui-box mw-ui-box-content">
  <div class="mw-bakers-list"></div>
  <div id="bkview"></div>
</div>

