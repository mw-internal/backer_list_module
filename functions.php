<?php

api_expose('get_backers_json');
function get_backers_json($page_params = array())
{
    ini_set('memory_limit', '2048M');
    $params = array();
    $params['table'] = "backers_list";
    $params['backer_visible'] = 1;
     if(Config::get('database.connections.backers.username')){
		   $params['connection_name'] = 'backers'; 
	   }

    $params['fields'] = "id,backer_name,backer_amount,backer_visible";

    if (isset($page_params['keyword']) and trim($page_params['keyword']) != '') {
        $params['search_in_fields'] = "backer_name,backer_email,backer_amount";
        $params['keyword'] = $page_params['keyword'];
    }
    //$params['current_page'] = 1;
    if (isset($page_params['current_page'])) {
        $params['current_page'] = $page_params['current_page'];
    }
    if (isset($page_params['limit'])) {
        $params['limit'] = $page_params['limit'];
    }

  if (isset($page_params['id'])) {
        $params['id'] = $page_params['id'];
    }


    $backers = array();

    $get = db_get($params);


    if ($get) {
        foreach ($get as $backer) {
            $cls = '1';
            $bv = intval($backer['backer_amount']);
			$bvis = intval($backer['backer_visible']);
			
			if($bvis != 0){
			
            if ($bv >= 5 and $bv <= 30) {
                $cls = 1;
            } else if ($bv >= 31 and $bv <= 50) {
                $cls = 2;
            } else if ($bv >= 51 and $bv <= 100) {
                $cls = 3;
            } else if ($bv >= 101 and $bv <= 500) {
                $cls = 4;
            } else if ($bv >= 501) {
                $cls = 5;
            }
            unset($backer['backer_amount']);
            $backer['level'] = $cls;
            $backers[] = $backer;
			}
        }
    }
    return $backers;
}

function get_backers($params = array())
{
    ini_set('memory_limit', '2048M');
    if (is_string($params)) {
        $params = parse_params($params);
    }
    $params['table'] = "backers_list";

    if (isset($params['keyword'])) {

        $params['search_in_fields'] = "backer_name,backer_email,backer_amount";

    }
	
	
	
	
	   $backers = array();
	   if(Config::get('database.connections.backers.username')){
		   $params['connection_name'] = 'backers'; 
	   }

    $get = db_get($params);
	
	if(!is_array($get)){
	return $get;	
	}
	
 if (isset($params['single'])) {
	 $get = array(0=>$get);
 }

    if ($get) {
        foreach ($get as $backer) {
			if(isset($backer['backer_amount'])){
				
            $cls = '1';
            $bv = intval($backer['backer_amount']);
			$bvis = intval($backer['backer_visible']);
			
			if($bvis != 0){
			
            if ($bv >= 5 and $bv <= 30) {
                $cls = 1;
            } else if ($bv >= 31 and $bv <= 50) {
                $cls = 2;
            } else if ($bv >= 51 and $bv <= 100) {
                $cls = 3;
            } else if ($bv >= 101 and $bv <= 500) {
                $cls = 4;
            } else if ($bv >= 501) {
                $cls = 5;
            }
            //unset($backer['backer_amount']);
            $backer['level'] = $cls;
            $backers[] = $backer;
			}
			} else {
				 $backer['level'] = 1;
            $backers[] = $backer;
			}
        }
    }
	 if (isset($params['single'])) {
	 return $backers[0];
 }
 
    return $backers;
	
	
	
    //return db_get($params);
}

api_expose_admin('save_backer');
function save_backer($data)
{
    $table = "backers_list";
    return db_save($table, $data);
}

api_expose_admin('delete_backer');
function delete_backer($params)
{
    if (isset($params['id'])) {
        $table = "backers_list";
        $id = $params['id'];
        return db_delete($table, $id);
    }
}