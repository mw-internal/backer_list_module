<?php
$config = array();
$config['name'] = "Backers List";
$config['author'] = "Microweber";
$config['ui'] = true; //module will be visible in the live edit
$config['ui_admin'] = true; //module will be visible in the admin
$config['categories'] = "other";
$config['position'] = 199;
$config['version'] = 0.14;

$config['tables'] = array(
    "backers_list" => array(
        'id' => "integer",
        'backer_name' => "text",
        'backer_email' => "text",
        'backer_amount' => "integer",
		'backer_visible' => array("type"=>'integer',"default"=>'1'),
        'created_at' => "dateTime"
    )
);
